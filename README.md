# LCCS Colour Scheme Test Notebook

The notebook will load in classified Level 4 as a netCDF and apply a colour scheme. It tries to replicate the functuality of the main code in a more interactive way.

To run need to install some additional libraries, can do this by installing conda (https://docs.conda.io/en/latest/miniconda.html) and creating a new environment using:

```
conda create -n dea-landcover -c conda-forge matplotlib jupyterlab xarray rasterio pyyaml
. activate dea-landcover
```
Start a Jupyter lab session using:
```
jupyter lab
```
This will open a new browser window with a Jupyter lab instance, from within this open the `lccs_colour_scheme_test.ipynb` file.